@ECHO OFF
:: Set RANDOM start time between 7:30 - 8:30
FOR /F "tokens=*" %%h IN ('powershell.exe -Command "Get-Random -Minimum 450 -Maximum 511"') DO (SET STARTTIME=%%h)
REM echo %STARTTIME%
:: Store the start HOUR as batch variable
FOR /F "tokens=*" %%i IN ('powershell.exe -Command "$startTime = new-timespan -minutes %STARTTIME%; $startTime.hours"') DO (SET HOUR=%%i)
REM echo %HOUR%
:: Store the start MINUTE as batch variable in 0:d2 format (two digit with leading 0 in case of one digit)
FOR /F "tokens=* usebackq" %%j IN (`powershell.exe -Command "$startTime = new-timespan -minutes %STARTTIME%; '{0:d2}' -f $startTime.minutes"`) DO (SET MINUTE=%%j)
REM echo %MINUTE%
:: Set RANDOM worktime between 8:12 - 8:27 (30 min lunch break included)
FOR /F "tokens=*" %%l IN ('powershell.exe -Command "Get-Random -Minimum 492 -Maximum 508"') DO (SET WORKTIME=%%l)
REM echo %WORKTIME%
:: Store the end HOUR as batch variable
FOR /F "tokens=*" %%k IN ('powershell.exe -Command "$endTime = %STARTTIME% + %WORKTIME% ; $endHour = new-timespan -minutes $endTime; $endHour.hours"') DO (SET ENDHOUR=%%k)
REM echo %ENDHOUR%
:: Store the end MINUTE as batch variable in 0:d2 format (two digit with leading 0 in case of one digit)
FOR /F "tokens=* usebackq" %%m IN (`powershell.exe -Command "$endTime = %STARTTIME% + %WORKTIME% ; $endMinute = new-timespan -minutes $endTime; '{0:d2}' -f $endMinute.minutes"`) DO (SET ENDMINUTE=%%m)
REM echo %ENDMINUTE%
@ECHO ON
cmd /k java -jar .\LogMeIn.jar -s %HOUR%:%MINUTE% -e %ENDHOUR%:%ENDMINUTE%
